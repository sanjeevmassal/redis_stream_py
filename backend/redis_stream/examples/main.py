import asyncio
from redis_stream.stream import RedisStream
from argparse import ArgumentParser
parser = ArgumentParser()


parser.add_argument('stream', help='Name of the stream',type=str)
parser.add_argument('group', help='Name of the group',type=str)
parser.add_argument('consumer', help='Name of the consumer',type=str)

args = parser.parse_args()


async def main(stream_name, group_name, consumer_id):
    redis_stream = await RedisStream.create(stream_name=stream_name, group_name=group_name, consumer_id=consumer_id)
    info = await redis_stream.get_stream_info()
    print(info)
    groups = await redis_stream.consumers_info()
    print(groups)
    try:
        async with redis_stream:
            async for result in redis_stream.get_stream_event():
                if result:
                    print(result)
                    await redis_stream.remove_item_from_stream(result.msgid)
    except asyncio.exceptions.CancelledError as e:
        print(e)

asyncio.run(main(args.stream, args.group, args.consumer))