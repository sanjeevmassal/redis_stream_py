import redis.asyncio as redis
import json
import logging

logging.basicConfig(filename="PubSubHelper.log", filemode="w", format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
datefmt="%d-%M-%Y %H:%M:%S", level=logging.WARNING)


class Redis(object):
    CONNECTION = []
    _CON_POOL = []

    def __init__(self):
        if not hasattr(Redis, 'pool'):
            Redis.getRedisCoon()  # get redis connection
        self._coon = redis.Redis(connection_pool=Redis.pool)

    @staticmethod
    def getRedisCoon():
        Redis.pool = redis.ConnectionPool(host='localhost', max_connections=10,decode_responses=True)


    async def get_connection(self) -> redis.Redis:
        conn = redis.Redis(connection_pool=redis.BlockingConnectionPool(host='localhost', max_connections=10))
        Redis.CONNECTION.append(conn)
        return conn

    def stream_info(self, channel):
        return self._coon.xinfo_stream(channel)

        # return subscribers specific info of a channel

    def channel_consumers_info(self, channel):
        InfoList = self._coon.xinfo_groups(channel)
        for GroupDict in InfoList:
            GroupDict.pop("consumers")
        return InfoList

    def create_consumer_group(self, name, channel):
        ret = self._coon.xgroup_create(channel, name, id="$")
        if ret == True:
            print(self.channel_consumers_info(channel))
        else:
            logging.error("create consumer %s fill,ret %s" % (name, ret))

    def publish(self, channel, msg):
        msgid = self._coon.xadd(channel, msg)
        return msgid

    def consumer_already_subscribed(self, channel, consumer):
        channel_consumers_infolist = self.channel_consumers_info(channel)
        for consumer_dict in channel_consumers_infolist:
            if consumer in consumer_dict.values():
                logging.warning("consumer %s has already subscribed %s" % (consumer, channel))
                return True
        return False

        # an exist consumer subscribe an exist channel

    def subscribe(self, name, channel, consumer):
        if (self.consumer_already_subscribed(channel, consumer)):
            return False
        self.create_consumer_group(name, channel)
        print(
        "%s subscribe %s success,channel %s info:" % (name, channel, channel), self.channel_consumers_info(channel))
        return

        # listen_channel and write into file

    def listen_channel(self, channel, consumer, file):
        if not (self.consumer_already_subscribed(channel, consumer)):
            return False
        mess = self._coon.xreadgroup(consumer, consumer, {channel: ">"})
        if mess != []:
            msg_list = mess[0][1]
            for msg in msg_list:
                id, content = msg[0], msg[1]
                content["msgid"] = id
                json_content = json.dumps(content)
                json_content += ","
                print("new message: ", content)
                with open(file, "a") as f:
                    f.write(json_content)
                self._coon.xack(channel, consumer, id)
