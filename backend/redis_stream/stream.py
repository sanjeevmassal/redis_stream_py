import typing

import redis.asyncio as redis
import asyncio
import os
import threading
import random
from enum import Enum
from typing import List, Union, AsyncIterable
import string
import logging
from redis.exceptions import ResponseError

from redis_stream.typed import StreamInfo

class MsgId(Enum):
    """
    '>' next undelivered messages in the group
    '0' get messages already added to consumer group
    """

    never_delivered = ">"
    already_deliverd = "0"


class RedisMsg:
    def __init__(self, stream, msgid, content):
        self.msgid = msgid
        self.stream = stream
        self.content = content

    def __str__(self):
        return f"id: {self.msgid}, stream={self.stream}, content: {self.content}"

    def __repr__(self):
        return f"RedisMsg(msgid={self.msgid}, stream={self.stream},  content={self.content})"


class RedisStream():
    _pool: redis.ConnectionPool | None = None
    _client: redis.Redis | None = None

    def __init__(self):
        self.consumer_id: Union[str, int, None] = None
        self.stream: Union[str, None] = None
        self.group: Union[str, None] = None
        self.logger: Union[logging.Logger, None] = None
        self.batch_size: Union[int, None] = None
        self.last_id = '>'

    def _random_str(self, n: int) -> str:
        return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(n))

    @classmethod
    async def create(cls, stream_name: str, group_name: str = None, batch_size: int = 1,
                     consumer_id: Union[str, int] = f"{os.getpid()}{threading.get_ident()}"
                     ) -> 'RedisStream':
        self = cls()
        self.consumer_id = consumer_id
        self.stream = stream_name
        self.decode_responses = True
        self.group = group_name if group_name else self._random_str(20)
        self.logger = logging.getLogger('redis-streams')
        self.batch_size = batch_size
        if not RedisStream._client:
            pool = RedisStream.get_pool()
            RedisStream._client = redis.Redis.from_pool(pool)
        await self.create_consumer_group(self.stream, self.group)
        return self

    async def set_last_id(self):
        consumers = await self.consumers_info()
        for consumer in consumers:
            if consumer['name'] == self.group:
                self.last_id = consumer['last-delivered-id']
        return self.last_id

    @staticmethod
    async def get_client() -> redis.Redis:
        if not RedisStream.client:
            raise Exception('client not initialized')
        pool = RedisStream.get_pool()
        client = redis.Redis.from_pool(pool)
        return client

    @staticmethod
    def get_pool():
        if not RedisStream._pool:
            RedisStream._pool = redis.ConnectionPool.from_url("redis://localhost?db=0", decode_responses=True)
        return RedisStream._pool

    async def publish(self, data):
        result = await self.client.xadd(self.stream, data)
        return result

    async def consumers_info(self):
        try:
            lists = await self.client.xinfo_groups(self.stream)
            return lists
        except Exception as e:
            print(e)

    async def read(self):
        data = await self.client.xread(count=self.batch_size, streams={self.stream: 0})
        return self._transform_redis_resp_to_objects(data)

    async def create_consumer_group(self, name, group):
        try:
            return await self.client.xgroup_create(name=name, groupname=group, id="$",mkstream=True)
        except ResponseError as e:
            print(f"raised: {e}")

    @property
    def client(self) -> redis.Redis:
        return RedisStream._client

    async def get_stream_info(self) -> StreamInfo:
        return await self.client.xinfo_stream(self.stream)

    @property
    def pool(self) -> redis.ConnectionPool:
        return RedisStream._pool

    async def close(self):
        return await self.client.close()

    def _get_no_of_messages_already_assigned(self):
        messages = self.get_pending_items(
            item_count=self.batch_size
        )
        _return = len(messages)
        self.logger.debug(f"Messages already assigned to this consumer: <= {_return}")
        return _return

    async def remove_item_from_stream(self, item_id: str):
        return await self.client.xack(self.stream, self.group, str(item_id))

    async def get_pending_items(self) -> List[dict]:
        return await self.client.xpending_range(
            name=self.stream,
            groupname=self.group,
            min="-",
            max="+",
            count=self.batch_size,
            consumername=self.consumer_id,
        )

    async def remove_consumer(self) -> int:
        return await self.client.xgroup_delconsumer(
            name=self.stream,
            groupname=self.group,
            consumername=self.consumer_id,
        )

    def _transform_redis_resp_to_objects(self, items):
        msgs = []
        if isinstance(items, list) and len(items):
            try:
                if str(items[0][0]) == self.stream:
                    items = items[0][1]
            except IndexError:
                self.logger.warning(
                    "Failed to process messages. Did you set  "
                    "of the Redis connection",
                    exc_info=True,
                )
        for item in items:
            [stream, [[msgid, parts]]] = item
            content = (p.decode('utf-8') for p in parts)
            msgs.append(RedisMsg(stream=stream.decode('utf-8'), msgid=msgid.decode('utf-8'), content=content))
        return msgs

    async def get_stream_event(self, last_id=None) -> AsyncIterable[RedisMsg]:
        while True:
            try:
                resp = await self.client.xreadgroup(streams={self.stream: self.last_id},
                                                    consumername=self.consumer_id,
                                                    count=self.batch_size,
                                                    groupname=self.group,
                                                    block=10)
                if resp:
                    resp = [res for res in resp if not len(res[1]) == 0]
                    for res in resp:
                        for strm in res[1]:

                            yield RedisMsg(stream=res[0], msgid=strm[0], content=strm[1])
                yield None
            except Exception as e:
                print(e)
            await asyncio.sleep(10)

    async def cleanup(self):
        await self.remove_consumer()

    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        await self.cleanup()


class RedisStreamConsumer(RedisStream):
    pass


class RedisStreamProducer(RedisStream):
    pass


class RedisStreamMonitor(RedisStream):
    pass
