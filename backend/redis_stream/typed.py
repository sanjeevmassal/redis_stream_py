from typing import TypedDict, TypeVarTuple

StreamInfo = TypedDict('StreamInfo', {
    'length': int,
    'first-entry': tuple,
    'groups': int,
    'last-entry': tuple | None,
    'last-generated-id': str,
    'radix-tree-nodes': int,
    'radix-tree-keys': int
})


__all__ = [
   'StreamInfo'
]